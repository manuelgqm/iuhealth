import getComponent from './getComponent'

describe('AlertBox', () => {
  let component = getComponent('AlertBox')

  it("displays nothing by default", () => {
    let error_text = component.shadowroot.querySelector('div').innerHTML

    expect(error_text).toBe("")
  })

  it("displays error message when validation error notified", () => {
    let error_text = "Validation error"
    
    component.bus.publish("validation.error", error_text)
    let actual_error_text = component.shadowroot.querySelector('div').innerHTML

    expect(error_text).toBe(actual_error_text)
  })

})