import getComponent from './getComponent'

describe('ItemsList', () => {
  let component = getComponent('ItemsList')

  it('starts with a empty list', () => {
    let rendered_items = getRenderedItems()

    expect(isListHidden()).toBeTruthy()
    expect(rendered_items).toHaveLength(0)
  })

  it('renders items received', () => {
    let first_item = { name: 'item 1', type: 'type 1' }
    let second_item = { name: 'item 2', type: 'type 2' }
    let items = []
    items.push(first_item)
    items.push(second_item)


    component.setAttribute("items", JSON.stringify(items))
    let rendered_items = getRenderedItems()

    expect(isListHidden()).toBeFalsy()
    expect(rendered_items).toHaveLength(2)
  })

  let isListHidden = () => component.elements.thead.classList.contains('hidden')
  let getRenderedItems = () => component.shadowroot.querySelectorAll('tbody tr')
})