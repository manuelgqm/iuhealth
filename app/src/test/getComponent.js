export default function getComponent(name) {
  let Component = require(`../components/${name}`).default

  let component = document.createElement('component-tag')
  document.querySelector('body').appendChild(component)
  Component.define('component-tag')

  return component
}
