import getComponent from './getComponent'

describe("Name input", () => {
  let component = getComponent('NameInput')

  it("should publish a bus message when value is changed", () => {
    let keyup = new Event('keyup')
    let spy = jest.spyOn(component.bus, 'publish')

    component
      .shadowroot.querySelector('#name')
      .dispatchEvent(keyup)

    expect(spy).toHaveBeenCalled()
  })

  it("should clear input value when item.saved message is received", () => {
    let input_element = component.shadowroot.querySelector("#name")
    input_element.value = "sample"

    component.bus.publish("item.saved")

    expect(input_element.value).toBe("")
  })
})