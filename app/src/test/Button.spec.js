import getComponent from './getComponent'

describe('DeleteButton', () => {
  let component = getComponent('Button')

  it("has a default label", () => {
    let default_label = "Aceptar"
    let rendered_label = component.elements.button.innerHTML

    expect(rendered_label).toBe(default_label)
  })

  it("can have a custom backgroud color", () => {
    let bg_color = "green"
    component.setAttribute("bg-color", "green")
    let actual_bg_color = component.elements.button.style['background-color']
    
    expect(actual_bg_color).toBe(bg_color)
  })

  it("can have a custom label", () => {
    let custom_label = "my button"
    component.setAttribute("label", custom_label)
    let rendered_label = component.elements.button.innerHTML

    expect(rendered_label).toBe(custom_label)
  })

  it("should publish given bus message when clicked ", () => {
    let click = new Event('click')
    let message = "button.clicked"
    let spy = jest.spyOn(component.bus, "publish")
    component.setAttribute("message", message)

    component.elements.button.dispatchEvent(click)

    expect(spy).toHaveBeenCalledWith(message)
  })
})