import getComponent from './getComponent'

describe("TypeDropdown", () => {
  let component = getComponent('TypeDropdown')
  let options_data = []

  beforeEach(() => {
    options_data = [{
      id: 1,
      value: 'value 1'
    }, {
      id: 2,
      value: 'value 2'
    }]

  })

  it("renders dropdown items", () => {
    let default_option = component.shadowroot.querySelectorAll('option')
  
    component.setAttribute("types", JSON.stringify(options_data))
    let options = component.shadowroot.querySelectorAll('option')

    expect(default_option.length).toBe(1)
    expect(options.length).toBe(3)
  })

  it("should publish a message when option changes", () => {
    let change = new Event('change')
    let spy = jest.spyOn(component.bus, 'publish')

    component.elements.select.dispatchEvent(change)

    expect(spy).toHaveBeenCalled()
  })

  it("should set default option when save.button.clicked received", () => {
    component.setAttribute("types", JSON.stringify(options_data))
    component.elements.select.value = 1
    component.bus.publish("save.button.clicked")

    expect(component.elements.select.value).toBe("")
  })

})