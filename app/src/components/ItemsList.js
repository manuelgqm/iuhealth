import ReactiveComponent from '../infrastructure/ReactiveComponent'

let template = /*html*/`
<style>
  div { margin-top: 15px}
  .hidden {
    display: none;
  }
  table {
    border-spacing: 0px;
    border-collapse: collapse;
  }
  th { 
    border-bottom: 3px solid cyan;
    border-rigth: 0px;
    padding: 6px;
  }
  td {
    padding: 6px;
    border-bottom: 1px solid Fuchsia;
  }
</style>

<div>
  <table>
    <thead class="hidden">
      <th>Nombre</th>
      <th>Tipo</th>
      <tbody></tbody>
    </thead>
  </table>
</div>
`

export default class ItemsList extends ReactiveComponent {
  constructor() {
    super(template)
    this.pickElements(['table', 'thead'])
  }
  
  static get observedAttributes() {
    return ['items']
  }
  
  render() {
    let items = JSON.parse(this.getAttribute('items'))
    this.shadowroot.querySelector('tbody').remove()
    
    if (!items.length) {
      this.elements.thead.classList.add('hidden')
      return
    }
    
    this.elements.thead.classList.remove('hidden')

    let new_tbody = this.getTbody(items)
    this.elements.table.appendChild(new_tbody)
  }

  getTbody(items) {
    let tbody = document.createElement('tbody')

    items.forEach(item => {
      let row = this.getRow(item)
      tbody.appendChild(row)
    })

    return tbody
  }

  getRow(item){
    let row = document.createElement('tr')
    let name_td = document.createElement('td')
    let type_td = document.createElement('td')

    name_td.innerHTML = item.name
    type_td.innerHTML = item.type.name

    row.appendChild(name_td)
    row.appendChild(type_td)
    
    return row
  }
  
}