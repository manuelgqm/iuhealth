import ReactiveComponent from '../infrastructure/ReactiveComponent'
import { myBus } from '../infrastructure/myBus'

let template = /*html*/`
<style>
  button {
    background-color: crimson;
    font-size: 12pt;
    border: none;
    display: inline;
    padding: 5px;
  }
  </style>

  <button>Aceptar</button>
`

export default class Button extends ReactiveComponent {
  constructor(){
    super(template, myBus)

    this.pickElements(['button'])

    this.elements.button.addEventListener('click', () => {
      let message = this.getAttribute("message")
      this.bus.publish(message)
    })
  }

  static get observedAttributes() {
    return ['label', 'bg-color']
  }

  render() {
    let button = this.elements.button
    let label = this.getAttribute("label")    
    button.style["background-color"] = this.getAttribute("bg-color")
    if (label) {
      this.elements.button.innerHTML = this.getAttribute("label")    
    }
  }
} 