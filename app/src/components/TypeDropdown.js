import ReactiveComponent from '../infrastructure/ReactiveComponent'
import { myBus } from '../infrastructure/myBus'

let template = /*html*/`
  <style>
    .field { margin-top: 10px;}
  </style>

  <div class="field">
    <label>Tipo:</label>
    <select>
      <option value="">selecciona uno...</option>
    </select>
  </div>
`
export default class TypeDropdown extends ReactiveComponent {
  constructor() {
    super(template, myBus)
    this.pickElements(['select'])
    
    this.elements.select.addEventListener('change', 
      event => { 
        let select = event.target
        let type = {
          id: select.value,
          name: select.options[select.selectedIndex].text
        }
        this.bus.publish('type.changed', type)
      }
    )

    this.bus.subscribe('save.button.clicked', () => this.elements.select.value = "")

  }

  static get observedAttributes() {
    return ['types']
  }

  render() {
    let types = JSON.parse(this.getAttribute('types'))
    types.forEach(item => {
      let option = document.createElement("option")
      option.value = item.id
      option.innerHTML = item.value
      this.elements.select.appendChild(option)
    })
  }
}