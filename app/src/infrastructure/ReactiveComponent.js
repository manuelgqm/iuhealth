import Component from './Component'

export default class ReactiveComponent extends Component {
  constructor(template, bus){
    super(template, bus)
  }

  attributeChangedCallback() {
    this.render()
  }

  render() {
    // implementation must be overwriten
  }
}