export default class Component extends HTMLElement {
  constructor(template, bus){
    super()
    this.shadowroot = this.attachShadow({ mode: 'open' })

    let template_element = document.createElement("template")
    template_element.innerHTML = template
    this.shadowroot.appendChild(template_element.content.cloneNode(true))
    
    this.bus = bus
    this.elements = {}
  }

  pickElements(element_selectors = []) {
    //TODO: convert dashed elements to camelCase
    //TODO: add support to queried by class
    element_selectors.forEach(
      selector => {
        let selector_name = selector
        if (selector.substring(0, 1) == '#') {
          selector_name = selector.substring(1, selector.length)
        }
        
        this.elements[selector_name] = this.shadowroot.querySelector(selector)
      }
    )
  }

  static mount(name){
    window.customElements.define(name, this)
  }
  
  static define(name) {
    window.customElements.get(name) || window.customElements.define(name, this)
  }
}