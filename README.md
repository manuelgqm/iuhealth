# IU Health
A puppy application about personal health tracking

## Dependencies
- Docker
- Docker-compose

## Start
- `docker-compose up`

## Test
- `docker-compose exec app npm test`

## Browse
- http://localhost:8181
